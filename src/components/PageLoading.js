import React from 'react';
import {LoopCircleLoading} from 'react-loadingg';

function PageLoading(props) {
    return (
        <div className="SiteLoader">
            <LoopCircleLoading size={"large"} color={"#045EA0"}/>
        </div>
    );
}

export default PageLoading;