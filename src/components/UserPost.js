import React from 'react';
import PostImage from '../img/post-new.PNG'
import {useSelector} from "react-redux";
import {useHistory} from 'react-router-dom'
import rehypeRaw from "rehype-raw";
import ReactMarkdown from "react-markdown";

const UserPost = ({userPost}) => {

    const {user} = useSelector(state=>state.post);

    const history = useHistory();

    return (
        <div className="col-md-4 mb-3">
            <div className="card h-100 user-post-card" onClick={()=>history.push(`/user/${user.id}/posts/${userPost.id}`)}>
                <div className="card-header bg-white">
                    <img className="img-fluid" src={PostImage} alt=""/>
                </div>
                <div className="card-body">
                    <ReactMarkdown className="post-title mb-3" children={userPost.title} rehypePlugins={[rehypeRaw]}/>
                    <ReactMarkdown className="post-description" children={userPost.body} rehypePlugins={[rehypeRaw]}/>
                    <div className="post-author">Author: <span>{user?.name}</span></div>
                </div>
            </div>
        </div>
    );
};

export default UserPost;