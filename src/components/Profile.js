import React, {Component} from 'react';
import Layout from "./Layout";
import {Link} from "react-router-dom";
import Avatar from "../img/avatar-4.jpg";
import AvatarMessage from "../img/avatar-5.jpg";
import AvatarMessage2 from "../img/avatar.jpg";
import AvatarMessage3 from "../img/avatar-4.jpg";
import AvatarMessage4 from "../img/avatar-2.jpg";
import MessageImg1 from "../img/unsplash-1.jpg";
import MessageImg2 from "../img/unsplash-2.jpg";
import AboutImg1 from "../img/webhome.png";
import AboutImg2 from "../img/github.png";
import AboutImg3 from "../img/location.png";
import "../style/Users.scss";
import "../style/mask_icon.scss"

class Profile extends Component {
    render() {
        return (
            <Layout>
                <div className="Profile">
                    <div className="">
                        <div className="row">
                            <div className="col-md-12">
                                <div className="page-title">
                                    Profile <Link to="/">Get more page examples</Link>
                                </div>
                            </div>
                        </div>

                        <div className="row">
                            <div className="col-md-4">
                                <div className="profile-card-left">

                                    <div className="Detail">
                                        <div className="title">Profil Details</div>

                                        <div className="avatar-box">
                                            <div className="img-box">
                                                <img src={Avatar} alt=""/>
                                            </div>
                                            <div className="name">Christina Mason</div>
                                            <div className="work">Lead Developer</div>

                                            <div className="button-box">
                                                <button>Follow</button>
                                                <button>
                                                    <img src="" alt=""/>
                                                    Message
                                                </button>
                                            </div>
                                        </div>
                                    </div>

                                    <div className="skills-box">
                                        <div className="title">
                                            Skills
                                        </div>
                                        <div className="skill-container">
                                            <Link to="/">HTML</Link>
                                            <Link to="/">JavaScript</Link>
                                            <Link to="/">Sass</Link>
                                            <br/>
                                            <Link to="/">Angular</Link>
                                            <Link to="/">Vue</Link>
                                            <Link to="/">React</Link>
                                            <Link to="/">Redux</Link>
                                            <br/>
                                            <Link to="/">UI</Link>
                                            <Link to="/">UX</Link>
                                        </div>
                                    </div>

                                    <div className="About-box">
                                        <div className="title">
                                            About
                                        </div>

                                        <div className="information">
                                            <img src={AboutImg1} alt=""/>
                                            <span> Lives in <a href="">San Francisco, SA</a></span>
                                        </div>
                                        <div className="information">
                                            <img src={AboutImg2} alt=""/>
                                            <span> Works at <a href="">Github</a></span>
                                        </div>
                                        <div className="information">
                                            <img src={AboutImg3} alt=""/>
                                            <span> From <a href="">Boston</a></span>
                                        </div>
                                    </div>

                                    <div className="Elsewhere">
                                        <div className="title">Elsewhere</div>
                                        <div className="link-box">
                                            <a href="">staciehall.co</a>
                                            <br/>
                                            <a href="">Twitter</a>
                                            <br/>
                                            <a href="">Facebook</a>
                                            <br/>
                                            <a href="">Instagram</a>
                                            <br/>
                                            <a href="">LinkedIn</a>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div className="col-md-8">
                                <div className="profile-card-right">
                                    <div className="title">
                                        Activities
                                    </div>

                                    <div className="message-container">

                                        <div className="message-box">
                                            <div className="left">
                                                <img src={AvatarMessage} alt=""/>
                                            </div>
                                            <div className="right">
                                                <div className="contact-about">
                                                    <div>
                                                        <div className="message-title">
                                                            <span>Vanessa Tucker </span>

                                                            started following <span>Christina Mason</span>
                                                        </div>
                                                        <div className="time">
                                                            Today 7:51 pm
                                                        </div>
                                                    </div>
                                                    <div className="date">5m ago</div>
                                                </div>
                                            </div>
                                        </div>

                                        <div className="message-box">
                                            <div className="left">
                                                <img src={AvatarMessage2} alt=""/>
                                            </div>
                                            <div className="right">
                                                <div className="contact-about">
                                                    <div>
                                                        <div className="message-title">
                                                            <span>Charles Hall </span>
                                                            posted something on <span>Christina Mason's</span> timeline
                                                        </div>
                                                        <div className="time">
                                                            Today 7:21 pm
                                                        </div>
                                                    </div>
                                                    <div className="date">30m ago</div>
                                                </div>

                                                <div className="message-text-img-box">
                                                    <div className="text">
                                                        Etiam rhoncus. Maecenas tempus, tellus eget condimentum rhoncus,
                                                        sem quam semper libero, sit amet adipiscing sem neque sed ipsum.
                                                        Nam quam nunc, blandit vel, luctus pulvinar, hendrerit id,
                                                        lorem. Maecenas nec odio et ante tincidunt tempus. Donec vitae
                                                        sapien ut libero venenatis faucibus. Nullam quis ante.
                                                    </div>
                                                    <button>
                                                        <div className="icon-sidebar icon-like"></div>
                                                        Like
                                                    </button>
                                                </div>
                                            </div>
                                        </div>

                                        <div className="message-box">
                                            <div className="left">
                                                <img src={AvatarMessage3} alt=""/>
                                            </div>
                                            <div className="right">
                                                <div className="contact-about">
                                                    <div>
                                                        <div className="message-title">
                                                            <span>Christina Mason </span>
                                                            posted a new blog
                                                        </div>
                                                        <div className="time">
                                                            Today 6:35 pm
                                                        </div>
                                                    </div>
                                                    <div className="date">1h ago</div>
                                                </div>
                                            </div>
                                        </div>

                                        <div className="message-box">
                                            <div className="left">
                                                <img src={AvatarMessage4} alt=""/>
                                            </div>
                                            <div className="right">
                                                <div className="contact-about">
                                                    <div>
                                                        <div className="message-title">
                                                            <span>William Harris </span>
                                                            posted two photos on
                                                            <span>Christina Mason's </span> timeline
                                                        </div>
                                                        <div className="time">
                                                            Today 5:12 pm
                                                        </div>
                                                    </div>
                                                    <div className="date">3h ago</div>
                                                </div>

                                                <div className="message-text-img-box">
                                                    <div className="message-img-box">
                                                        <img src={MessageImg1} alt=""/>
                                                        <img src={MessageImg2} alt=""/>
                                                    </div>
                                                    <button>
                                                        <span className="icon icon-like"></span>
                                                        Like
                                                    </button>
                                                </div>
                                            </div>
                                        </div>

                                        <div className="message-box">
                                            <div className="left">
                                                <img src={AvatarMessage4} alt=""/>
                                            </div>
                                            <div className="right">
                                                <div className="contact-about">
                                                    <div>
                                                        <div className="message-title">
                                                            <span>William Harris </span>
                                                            started following
                                                            <span> Christina Mason</span>
                                                        </div>
                                                        <div className="time">
                                                            Yesterday 3:12 pm
                                                        </div>
                                                    </div>
                                                    <div className="date">1d ago</div>
                                                </div>

                                                <div className="message-text-img-box">
                                                    <div className="following-box">
                                                        <div className="left">
                                                            <img src={AvatarMessage3} alt=""/>
                                                        </div>
                                                        <div className="right">
                                                            <div className="text">
                                                                Nam quam nunc, blandit vel, luctus pulvinar, hendrerit
                                                                id, lorem. Maecenas nec odio et ante tincidunt tempus.
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div className="message-box">
                                            <div className="left">
                                                <img src={AvatarMessage3} alt=""/>
                                            </div>
                                            <div className="right">
                                                <div className="contact-about">
                                                    <div>
                                                        <div className="message-title">
                                                            <span>Christina Mason </span>
                                                            posted a new blog
                                                        </div>
                                                        <div className="time">
                                                            Yesterday 2:43 pm
                                                        </div>
                                                    </div>
                                                    <div className="date">1d ago</div>
                                                </div>
                                            </div>
                                        </div>

                                        <div className="message-box">
                                            <div className="left">
                                                <img src={AvatarMessage2} alt=""/>
                                            </div>
                                            <div className="right">
                                                <div className="contact-about">
                                                    <div>
                                                        <div className="message-title">
                                                            <span>Charles Hall </span>
                                                            started following
                                                            <span> Christina Mason</span>
                                                        </div>
                                                        <div className="time">
                                                            Yesterday 1:51 pm
                                                        </div>
                                                    </div>
                                                    <div className="date">1d ago</div>
                                                </div>
                                            </div>
                                        </div>

                                    </div>

                                    <button className="btn-block">Load more</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </Layout>
        );
    }
}

export default Profile;