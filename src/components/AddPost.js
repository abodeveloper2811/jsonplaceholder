import React, {useState} from 'react';
import Layout from "./Layout";
import PostImage from '../img/create-post.jpeg'
import {AvField, AvForm} from "availity-reactstrap-validation";
import {useDispatch, useSelector} from "react-redux";
import ReactMarkdown from 'react-markdown';
import rehypeRaw from 'rehype-raw'
import {addNewPost, setLoading} from "../redux/actions/postActions";
import {useHistory} from 'react-router-dom';
import MarkDownImage from '../img/markdown.PNG';
import "../style/AddPost.scss"

const AddPost = () => {

    const {users} = useSelector(state => state.post);
    const [title, setTitle] = useState();
    const [body, setBody] = useState();

    const history = useHistory();

    const dispatch = useDispatch();

    const changePostTitle = (event) => {
        setTitle(event.target.value)
    };

    const changePostBody = (event) => {
        setBody(event.target.value)
    };

    const handleSubmit = (event, value) => {
        dispatch(setLoading());
        dispatch(addNewPost(value));
        history.push(`/user/${value.userId}/posts`);
    };

    return (
        <Layout>
            <div className="Add-post">
                <div className="title">Create Post</div>

                <div className="row">
                    <div className="col-md-6">
                        <div className="card markdown-card">
                            <div className="card-header p-0">
                                <img className="img-fluid" src={MarkDownImage} alt=""/>
                            </div>
                            <div className="card-body">
                                <AvForm className="w-100" onValidSubmit={handleSubmit}>

                                    <AvField
                                        className="form-control post-title"
                                        type="textarea"
                                        label="Post title"
                                        placeholder="Enter you post title ..."
                                        name="title"
                                        onChange={changePostTitle}
                                        required
                                    />

                                    <AvField
                                        className="form-control post-body"
                                        label="Post body"
                                        type="textarea"
                                        placeholder="Enter you post body ..."
                                        name="body"
                                        onChange={changePostBody}
                                        required
                                    />

                                    <AvField
                                        className="form-control"
                                        type="select"
                                        name="userId"
                                        label="Author"
                                        helpMessage=""
                                        required
                                        selected={true}
                                    >
                                        <option value=""
                                                disabled={true}
                                        >Choose the author !!!
                                        </option>
                                        {
                                            users.map((item, index) => (
                                                <option value={item.id}>{item.name}</option>
                                            ))
                                        }
                                    </AvField>


                                    <button
                                        type="submit"
                                        className="btn btn-success btn-block"
                                    >CREATE POST
                                    </button>


                                </AvForm>
                            </div>
                        </div>
                    </div>

                    <div className="col-md-6">
                        <div className="card show-card">
                            <div className="card-header p-0">
                                <img src={PostImage} alt="" className="img-fluid"/>
                            </div>
                            <div className="card-body">

                                <div className="post-show">Show Post title:</div>
                                <ReactMarkdown className="show-post-title" children={title} rehypePlugins={[rehypeRaw]}/>

                                <div className="post-show">Show Post body:</div>
                                <ReactMarkdown className="show-post-body" children={body} rehypePlugins={[rehypeRaw]} />

                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </Layout>
    );
};

export default AddPost;