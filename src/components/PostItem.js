import React, {useEffect, useRef} from 'react';
import Layout from "./Layout";
import {useDispatch, useSelector} from "react-redux";
import {addPostComment, getPostComment, getPostItem} from "../redux/actions/postActions";
import {useParams} from 'react-router-dom';
import PostImage from '../img/post-new.PNG';
import "../style/PostItem.scss";
import Person from '../img/avatars/1.png'
import {AvField, AvForm} from "availity-reactstrap-validation";
import ReactMarkdown from "react-markdown";
import rehypeRaw from "rehype-raw";
import PageLoading from "./PageLoading";

const PostItem = () => {

    const dispatch = useDispatch();
    const params = useParams();
    const myForm = useRef();

    useEffect(()=>{
        dispatch(getPostItem(params.postID));
        dispatch(getPostComment(params.postID));
    },[]);

    const {post, user, comments, loading} = useSelector(state=>state.post);

    const handleSubmit = (event, value) => {

        let comment = {};

        comment = {
            postId: post.id,
            name: user.name,
            email: user.email,
            body: value.body
        };

        dispatch(addPostComment(comment));

        myForm.current.reset();
    };

    return (
        <Layout>

            {
                loading ? <PageLoading/> : <div className="Post-item">

                    <div className="row">
                        <div className="col-md-8">
                            <div className="card user-post-card">
                                <div className="card-header">
                                    <img className="img-fluid w-100" src={PostImage} alt=""/>
                                </div>
                                <div className="card-body">

                                    <ReactMarkdown className="post-title mb-3" children={post?.title} rehypePlugins={[rehypeRaw]}/>
                                    <ReactMarkdown className="post-description" children={post?.body} rehypePlugins={[rehypeRaw]}/>

                                    <div className="post-author">Author: <span>{user?.name}</span></div>
                                </div>
                            </div>
                        </div>
                        <div className="col-md-4">
                            <div className="card-title">Comments to the post</div>
                            <div className="card commnet-box">
                                <div className="card-body">
                                    {comments?.map((comment, index)=>(
                                        <div className="comment-item mt-2">
                                            <div className="top">
                                                <img src={Person} alt=""/>
                                                <div className="comment-email">{comment.email}</div>
                                            </div>
                                            <div className="comment-description">
                                                {comment.body}
                                            </div>
                                        </div>
                                    ))}

                                    <div className="add-comment">

                                        <div className="add-comment-title">Add comment</div>

                                        <AvForm ref={myForm} className="w-100" onValidSubmit={handleSubmit}>

                                            <AvField
                                                className="form-control"
                                                type="textarea"
                                                placeholder="Enter you comment ..."
                                                name="body"
                                            />

                                            <button type="submit" className="btn-danger btn">SEND</button>

                                        </AvForm>

                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            }

        </Layout>
    );
};

export default PostItem;