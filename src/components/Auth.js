import React from 'react';
import Avatar from "../img/avatar.jpg";
import {AvCheckbox, AvCheckboxGroup, AvField, AvForm} from "availity-reactstrap-validation";
import {Link} from "react-router-dom";
import {useDispatch, useSelector} from "react-redux";
import {SignUp} from "../redux/actions/userAction";
import {useHistory} from 'react-router-dom'

const Auth = () => {

    const dispatch = useDispatch();

    const history = useHistory();

    const {loading} = useSelector(state=>state.auth);

    const handleSubmit = (event, value) => {
        dispatch(SignUp(value, history));
    };

    const myStyle = {
        backgroundColor: '#EBEBFF'
    };

    return (
        <div className="Auth" style={myStyle}>
            <div className="container vh-100 d-flex flex-column justify-content-center">
                <div className="row">
                    <div className="col-md-6 h-100 mx-auto">
                        <div className="d-flex w-100 flex-column align-items-center justify-content-center">
                            <h3 className="title font-weight-bold text-center">
                                Get started
                            </h3>
                            <p className="commit text-center">
                                Start creating the best possible user experience for you customers.
                            </p>

                            <div className="card w-100 p-2">
                                <div className="card-body d-flex flex-column align-items-center justify-content-center">

                                    <AvForm className="w-100" onValidSubmit={handleSubmit}>

                                        <AvField
                                            className="form-control"
                                            type="text"
                                            label="Name"
                                            placeholder="Enter you name"
                                            name="name"
                                            required
                                        />

                                        <AvField
                                            className="form-control"
                                            label="Email"
                                            type="email"
                                            placeholder="Enter you email"
                                            name="email"
                                            required
                                        />

                                        <AvField
                                            className="form-control"
                                            label="Password"
                                            type="password"
                                            placeholder="Enter password"
                                            name="password"
                                            required
                                        />

                                        <AvField
                                            className="form-control"
                                            label="Confirm password"
                                            type="password"
                                            placeholder="Enter confirmation password"
                                            name="password_confirmation"
                                            required
                                        />

                                        <div className="text-center">
                                            <button
                                                disabled={loading}
                                                type="submit"
                                                className="btn btn-primary"
                                            >Sign up</button>
                                        </div>

                                    </AvForm>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default Auth;