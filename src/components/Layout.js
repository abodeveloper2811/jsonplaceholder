import React, {useEffect, useState} from 'react';
import "../style/Layout.scss";
import "../style/mask_icon.scss";
import "../style/Scrollbar.scss";
import {Dropdown, Menu} from 'antd';
import {DownOutlined} from '@ant-design/icons';
import Logout from '../img/logout.png'
import User from '../img/user.png'
import Avatar from '../img/avatars/4.png';
import {NavLink, useHistory} from "react-router-dom";

const Layout = (props) => {

    const [showSidebar, setShowSidebar] = useState(true);

    const history = useHistory();

    let screenwidth;

    useEffect(()=>{
        screenwidth = window.outerWidth;
    },[]);

    useEffect(()=>{
        if (screenwidth < 768){
            setShowSidebar( false);
            console.log('Ishladi');
        }
    }, []);

    const logOut = () =>{
        history.push('/users');
    };

    const menuProfil = (
        <Menu className="profil-menu">
            <Menu.Item key="0">
                <img src={User} alt=""/>
                <span>Profile</span>
            </Menu.Item>
            <Menu.Divider/>
            <Menu.Item key="1" onClick={()=>logOut()}>
                <img src={Logout} alt=""/>
                <span>Chiqish</span>
            </Menu.Item>
        </Menu>
    );

    const sidebarData = [
        {
            menuName: 'Posts',
            menuItem: [
                {
                    img: 'icon-sidebar icon-my-user',
                    name: 'Users',
                    pathName: '/users'
                },
                {
                    img: 'icon-sidebar icon-add-post',
                    name: 'Add post',
                    pathName: '/add-post'
                }
            ]
        }
    ];

    const hiddenMobileSidebar = () =>{
        if (window.innerWidth > 576){
            setShowSidebar( false);
        }
    };

    const setSidebar = (showSidebar) =>{
        setShowSidebar(!showSidebar)
    };

    return (
        <div className="Layout">

            <nav className="Navbar">
                <div className="nav-logo" onClick={()=>setSidebar(showSidebar)}>
                    <div className="icon icon-bars">

                    </div>
                </div>

                <ul>
                    <li className="user-profile">
                        <Dropdown overlay={menuProfil} trigger={['click']}>
                            <a className="ant-dropdown-link" onClick={e => e.preventDefault()}>
                                <img src={Avatar} alt=""/>
                                <span>Administrator</span>
                                <DownOutlined/>
                            </a>
                        </Dropdown>
                    </li>
                </ul>
            </nav>

            <div className="bottom">
                <div className={showSidebar ? 'sidebar': ' sidebar hidden-sidebar'}>
                    <div className="title">
                        Sidebar menu
                    </div>

                    <div className="menu-box">

                        {
                            sidebarData.map((item, index)=>(
                                <div className="menu">
                                    <div className="menu-name">
                                        {item.menuName}
                                    </div>
                                    <ul>
                                        {
                                            item.menuItem.map((link, linkIndex)=>(
                                                <li onClick={()=>hiddenMobileSidebar()}>
                                                    <NavLink activeClassName="active-link" to={link.pathName}>
                                                        <div className={link.img}></div>
                                                        <div className="link-name">{link.name}</div>
                                                    </NavLink>
                                                </li>
                                            ))
                                        }
                                    </ul>
                                </div>
                            ))
                        }
                    </div>

                </div>
                <div className={showSidebar ? 'content': 'content fluid-content'}>
                    <div>
                        {props.children}
                    </div>
                </div>

            </div>

        </div>
    );
};

export default Layout;