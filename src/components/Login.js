import React from 'react';
import Avatar from "../img/avatar.jpg";
import {AvCheckbox, AvCheckboxGroup, AvField, AvForm} from "availity-reactstrap-validation";
import {Link} from "react-router-dom";
import {useDispatch, useSelector} from "react-redux";
import {SignIn} from "../redux/actions/userAction";
import {useHistory} from 'react-router-dom'

const Login = () => {

    const dispatch = useDispatch();

    const history = useHistory();

    const {loading} = useSelector(state => state.auth);

    const handleSubmit = (event, value) => {
        dispatch(SignIn(value, history));
    };

    const myStyle = {
        backgroundColor: '#EBEBFF'
    };

    return (
        <div className="My-login" style={myStyle}>
            <div className="container vh-100 d-flex flex-column justify-content-center">
                <div className="row">
                    <div className="col-md-6 h-100 mx-auto">
                        <div className="d-flex w-100 flex-column align-items-center justify-content-center">
                            <h3 className="title font-weight-bold text-center">
                                XATP13 Ishlab chiqarish tizimi
                            </h3>
                            <h5 className="commit text-center">
                                Login va parolni terib kiring
                            </h5>

                            <div className="card w-100 p-2">
                                <div className="card-body d-flex flex-column align-items-center justify-content-center">
                                    <div className="text-center">
                                        <img src={Avatar} alt="Charles Hall"
                                             className="img-fluid rounded-circle" width="132"
                                             height="132"/>
                                    </div>

                                    <AvForm className="w-100" onValidSubmit={handleSubmit}>

                                        <AvField
                                            className="form-control"
                                            label="Email/Email"
                                            type="email"
                                            placeholder="Enter you email"
                                            name="email"
                                            required
                                        />

                                        <AvField
                                            className="form-control"
                                            label="Parol"
                                            type="password"
                                            placeholder="Enter you password"
                                            name="password"
                                            required
                                        />

                                        <AvCheckboxGroup name="name">
                                            <AvCheckbox
                                                className="form-check-input"
                                                label="Eslab qolish"
                                                value="checked"
                                                name="name"
                                                checked
                                            />
                                        </AvCheckboxGroup>

                                        <div className="text-center">
                                            <button
                                                disabled={loading}
                                                type="submit"
                                                className="btn btn-primary"
                                            >Tizimga kirish
                                            </button>
                                        </div>

                                        <div className="sign-up-box text-center mt-3">
                                            <h5 className="sign-up-title">Yangi foydalanuvchimisiz ?</h5>
                                            <Link to="/auth">Ro'yxatdan o'tish</Link>
                                        </div>

                                    </AvForm>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default Login;