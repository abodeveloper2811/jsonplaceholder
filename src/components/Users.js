import React, {useEffect} from 'react';
import Layout from "./Layout";
import {getPosts, getUsers} from "../redux/actions/postActions";
import {useDispatch, useSelector} from "react-redux";
import User from "./User";
import "../style/Users.scss";

const Users = () => {

    const dispatch = useDispatch();

    const {users} = useSelector(state=>state.post);

    useEffect(()=>{
        dispatch(getPosts());
    },[]);

    return (
        <Layout>
            <div className="Users">
                <div className="">
                    <div className="row">
                        <div className="col-md-12">
                            <div className="page-title">
                                Users
                            </div>
                        </div>
                    </div>

                    <div className="row">

                        {users?.map((user, index)=>(
                            <User user={user}/>
                        ))}

                    </div>
                </div>
            </div>
        </Layout>
    );
};

export default Users;