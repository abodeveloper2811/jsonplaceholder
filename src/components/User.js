import React from 'react';
import Location from "../img/location.png";
import Email from "../img/email.png";
import Telephone from "../img/telephone-call.png";
import {useHistory} from 'react-router-dom'

const User = ({user}) => {

    const history = useHistory();

    return (
        <div className="col-md-4 mb-3">
            <div className="user-card" onClick={()=>history.push(`/user/${user.id}/posts`)}>

                <div className="Detail">
                    <div className="title">User</div>

                    <div className="avatar-box">
                        <div className="img-box">
                            <img src={user.img} alt=""/>
                        </div>
                        <div className="username">{user.username}</div>
                        <div className="name">{user.name}</div>

                        <div className="button-box">
                            <button>Follow</button>
                        </div>
                    </div>
                </div>

                <div className="About-box">
                    <div className="title">
                        About
                    </div>

                    <div className="information">
                        <img src={Email} alt=""/>
                        <span>{user.email}</span>
                    </div>

                    <div className="information">
                        <img src={Location} alt=""/>
                        <span>{user.address.city}, {user.address.street}</span>
                    </div>

                    <div className="information">
                        <img src={Telephone} alt=""/>
                        <span>{user.phone}</span>
                    </div>

                </div>

            </div>
        </div>
    );
};

export default User;