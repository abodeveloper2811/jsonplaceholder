import React, {useEffect} from 'react';
import Layout from "./Layout";
import {useDispatch, useSelector} from "react-redux";
import {getUser, getUserPosts} from "../redux/actions/postActions";
import {useParams} from 'react-router-dom'
import UserPost from "./UserPost";
import "../style/UserPosts.scss";
import SiteLoader from "./SiteLoader";
import PageLoading from "./PageLoading";

const UserPosts = () => {

    const dispatch = useDispatch();
    const params = useParams();

    const {userPosts, loading} = useSelector(state=>state.post);

    useEffect(()=>{
        dispatch(getUserPosts(params.id));
        dispatch(getUser(params.id));
    },[]);

    return (
        <Layout>

            <div className="User-posts">
                <div className="row">
                    {loading ? <PageLoading/> : userPosts?.map((item, index)=>(
                        <UserPost userPost={item}/>
                    ))}
                </div>
            </div>


        </Layout>
    );
};

export default UserPosts;