import React, {useEffect, useState} from 'react';
import "./App.css";
import {Redirect, Route, Switch} from "react-router-dom";
import {ToastContainer} from "react-toastify";
import Users from "./components/Users";
import UserPosts from "./components/UserPosts";
import PostItem from "./components/PostItem";
import AddPost from "./components/AddPost";
import SiteLoader from "./components/SiteLoader";
import {useDispatch} from "react-redux";
import {getUsers} from "./redux/actions/postActions";

function App(props) {

    const [siteloading, setSiteLoading] = useState(true);
    const dispatch = useDispatch();

    useEffect(()=>{
        dispatch(getUsers());
        setSiteLoading(false);
    },[]);

    if (siteloading){
        return (
            <SiteLoader/>
            )
    }

    return (
        <>
            <Switch>

                <Route path="/users" exact component={Users}/>
                <Route path="/user/:id/posts" exact component={UserPosts}/>
                <Route path="/user/:id/posts/:postID" exact component={PostItem}/>
                <Route path="/add-post" exact component={AddPost}/>

                <Route>
                    <Redirect to="/users"/>
                </Route>

            </Switch>

            <ToastContainer/>
        </>

    );
}

export default App;