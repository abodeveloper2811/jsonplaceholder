import {
    ADD_NEW_POST,
    ADD_POST_COMMENT,
    GET_POST,
    GET_POST_COMMENT,
    GET_POSTS,
    GET_USER,
    GET_USER_POSTS,
    GET_USERS, LOADING_FALSE, SET_LOADING
} from "../actionTypes/userActionsTypes";
import axios from "axios";
import {toast} from 'react-toastify';

export const getUsers = () => async (dispatch) => {
    try {
        const res = await axios.get(`https://jsonplaceholder.typicode.com/users`);
        dispatch({
            type: GET_USERS,
            payload: res.data,
        });

    } catch (err) {
        console.log(err);
    }
};

export const getPosts = () => async (dispatch) => {

    dispatch(setLoading());

    try {
        const res = await axios.get(`https://jsonplaceholder.typicode.com/posts`);
        dispatch({
            type: GET_POSTS,
            payload: res.data,
        });

    } catch (err) {
        console.log(err);
    }
};

export const getUser = (userID) => async (dispatch) => {

    try {

        const res = await axios.get(`https://jsonplaceholder.typicode.com/users/${userID}`);
        dispatch({
            type: GET_USER,
            payload: res.data,
        });

    } catch (err) {
        console.log(err);
    }
};

export const getUserPosts = (id) => async (dispatch) => {

    dispatch(setLoading());

    try {

        dispatch({
            type: GET_USER_POSTS,
            payload: id
        });

    } catch (err) {
        console.log(err);
    }
};

export const getPostItem = (postID) => async (dispatch) => {

    dispatch(setLoading());

    try {
        const res = await axios.get(`https://jsonplaceholder.typicode.com/posts/${postID}`);
        dispatch({
            type: GET_POST,
            payload: res.data,
        });

    } catch (err) {
        console.log(err);
    }
};

export const getPostComment = (postID) => async (dispatch) => {

    try {
        const res = await axios.get(`https://jsonplaceholder.typicode.com/comments`);
        dispatch({
            type: GET_POST_COMMENT,
            payload: {
                postID: postID,
                allComments: res.data
            },
        });

    } catch (err) {
        console.log(err);
    }
};

export const addPostComment = (comment) => async (dispatch) => {

    try {
        const res = await axios.post(`https://jsonplaceholder.typicode.com/comments`, comment);

        dispatch({
            type: ADD_POST_COMMENT,
            payload: res
        });

    } catch (err) {
        console.log(err);
    }
};

export const addNewPost = (post) => async (dispatch) => {

    try {
        const res = await axios.post(`https://jsonplaceholder.typicode.com/posts`, post);

        toast.success('Post muvaffaqiyatli yaratildi !!!');

        dispatch({
            type: ADD_NEW_POST,
            payload: res.data
        });

    } catch (err) {
        console.log(err);
        toast.error('Post yaratilmadi !!!');
    }
};

export const setLoading = () => async (dispatch) => {

    try {

        dispatch({
            type: SET_LOADING
        });

    } catch (err) {
        console.log(err);
    }
};




