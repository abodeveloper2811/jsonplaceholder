export const GET_USERS = "GET_USERS";
export const GET_USER = "GET_USER";
export const GET_POSTS = "GET_POSTS";
export const GET_POST = "GET_POST";
export const ADD_NEW_POST = "ADD_NEW_POST";
export const GET_POST_COMMENT = "GET_POST_COMMENT";
export const ADD_POST_COMMENT = "ADD_POST_COMMENT";
export const GET_USER_POSTS = "GET_USER_POSTS";

export const SET_LOADING = "SET_LOADING";



