import {
    ADD_NEW_POST,
    ADD_POST_COMMENT,
    GET_POST,
    GET_POST_COMMENT,
    GET_POSTS,
    GET_USER,
    GET_USER_POSTS,
    GET_USERS, LOADING_FALSE, SET_LOADING
} from "../actionTypes/userActionsTypes";

const initialState = {
    users: [],
    user: {},
    posts: [],
    userPosts: null,
    post: null,
    comments: null,
    loading: true
};

export const postReducer = (prevState = initialState, action) => {

    const {type, payload} = action;

    switch (type) {

        case SET_LOADING:
            return {
                ...prevState,
                loading: true
            };

        case GET_USER:
            return {
                ...prevState,
                user: payload
            };

        case GET_USERS:

            const Avatars = [
                '/img/avatars/1.png',
                '/img/avatars/2.png',
                '/img/avatars/3.png',
                '/img/avatars/4.png',
                '/img/avatars/5.png',
                '/img/avatars/6.png',
                '/img/avatars/7.png',
                '/img/avatars/8.png',
                '/img/avatars/9.png',
                '/img/avatars/10.png',
            ];

            let newUsers = [];
            let newUser = {};


            for (let i=0; i<payload.length; i++){
                newUser  = {
                    ...payload[i],
                    img: Avatars[i]
                };
                newUsers.push(newUser);
            };

            console.log(newUsers);

            return {
                ...prevState,
                users: newUsers
            };

        case GET_POSTS:
            return {
                ...prevState,
                posts: payload,
                loading: false
            };

        case GET_POST:
            return {
                ...prevState,
                post: payload,
                loading: false,
            };

        case GET_POST_COMMENT:

            return {
                ...prevState,
                comments: payload.allComments.filter((item) => item.postId == payload.postID)
            };

        case ADD_POST_COMMENT:

            let newComments = [];

            newComments = [...prevState.comments];
            newComments.push(payload.data);

            return {
                ...prevState,
                comments: newComments
            };

        case ADD_NEW_POST:

            let newUserPosts = [];

            newUserPosts =  prevState.posts.filter((item) => item.userId == payload.userId);
            newUserPosts.push(payload);

            console.log(newUserPosts);

            return {
                ...prevState,
                userPosts: newUserPosts,
                loading: false
            };

        case GET_USER_POSTS:
            return {
                ...prevState,
                userPosts: prevState.posts.filter((item) => item.userId == payload),
                loading: false
            };

        default:
            return prevState;
    }
};